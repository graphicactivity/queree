// Inits
$(window).on('load', function() {
    $('.grid').isotope({
      itemSelector: '.gridItem',
      horizontalOrder: true,
      gutter: 0
    });
});
$(document).ready(function(){
    // Lightbox
    $( '.swipebox' ).swipebox({
        videoMaxWidth : 1440,
    });
    // Category Menu
    $('.categoryMenu').click(function(){
        $(this).toggleClass('open');
        $('.fullMenu').toggleClass('openMenu');
        $('body').toggleClass('fixHeader');
    });
    $('.openMobileMenu').click(function(){
        $(this).toggleClass('cross');
        $('.mobileNav').toggleClass('revealMobileMenu');
    });
    // Scroll animations
    var wow = new WOW(
        {
            boxClass: 'reveal'
        }
    );
    wow.init();
});
